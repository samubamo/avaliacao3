
import utfpr.ct.dainf.pratica.Poligonal;
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
        Poligonal<PontoXZ> pf = new Poligonal<>();
        pf.getVertices().add(new PontoXZ(-3, 2));
        pf.getVertices().add(new PontoXZ(-3, 6));
        pf.getVertices().add(new PontoXZ(0, 2));
        
        System.out.println("Comprimento da poligonal = " + pf.getComprimento());
    }
    
}
