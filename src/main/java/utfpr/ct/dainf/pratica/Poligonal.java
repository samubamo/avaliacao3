package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();

    public List<T> getVertices() {
        return vertices;
    }
    
    public int  getN(){
        /*int n_vertices = 0;
        for(T vertice: vertices){
            n_vertices++;
        }
        return n_vertices;*/
        return vertices.size();
    }
    
    public T get(int i){
        if(vertices.get(i) == null){
            throw new RuntimeException(String.format("get(%d): índice inválido", i));
        }
        return vertices.get(i);
    }
    
    public void set(int i, T p){
      // vertices.set(i,p);
       if(i < 0 || i > vertices.size()){
           throw new RuntimeException(String.format( "set(%d): índice inválido", i));
       }
       if(i == vertices.size()){
           vertices.add(i,p);
       }
       vertices.set(i,p);
    }
    
    public double getComprimento(){
        double soma = 0;
        
        for(int i = 0; i < vertices.size()- 1; i++){
            soma += vertices.get(i).dist(vertices.get(i));
        }
        return soma;
    }
    
        
    

}
